---
title: "GDB"
date: 2022-11-22T22:59:12+01:00
draft: true
showToc: true
---

The GNU Debugger (GDB) is a portable debugger that runs on many Unix-like systems and works for many programming languages, including Ada, C, C++, Objective-C, Free Pascal, Fortran, Go and partially others.

## Setup
### Set intel syntax
```
set disassembly-flavor intel
```

## Static Analysis
### Disassemble a function
```
disassemble functionname
```
### List functions
```
info functions
```
### List symbols
Useful to find the entry point
```
info file
```

## Visualization
### Layouts
#### Assembly Layout
Assembly code on the top, gdb prompt on the bottom
```
layout asm
```

## Execution
### Run the program (until a breakpoint is encountered)
```
run
```
### Start the program but stop at first instruction
```
starti
```
### Jump to an instruction (specified by an offset from a function)
```
jump *(functionname+offset)
```

## Breakpoints
### Set a breakpoint at an offset from a function
```
break *(functionname+offset)
```
> Example
```
break *(main+5)
```
### List breakpoints
```
info break
```
### delete breakpoint
```
del breakpointid
```

