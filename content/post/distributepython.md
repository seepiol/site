---
title: "Distribute Python Application: The Human-Friendly Way"
date: 2024-04-15T23:10:00+02:00
---

This is the story of my latest experience in the development field. 

If you want to jump to the conclusion, and if you have had the same tragedy as me and are looking for a solution, skip to the last point: [The Solution](#act-0xff-the-solution)

## ACT 0x1: The Vision

During my last project as a "freelance developer", I had to write a quite simple GUI application.

I decided to use Python with customtkinter: It's not the best, you're right, but it's the one I know and it just seemed ok for what I had to do.

In about a month I practically finished writing the business logic of the application, and up to this point everything went good. _too good._ 

At this point I had all my .py sources, but the project requirements had portability as an unquestionable requirement: I could not ask the IT-noob customer to install a python interpreter!

The obvious fix for this was [PyInstaller](https://pyinstaller.org/en/stable/). At first I decided to use it to generate a linux executable. The customer didn't care because he only had windows machines, but trying to use the tool seemed a good idea to me, also because PyInstaller doesn't support cross-compiling and so I would have had to setup a development env on a windows machine, and I wanted to delay that time as much as possible.

After a bit of fine tuning with the cli options, I managed to get an executable, and it worked fine! 
The only downside was that I had an .exe file, acting as a bootloader, with a folder containing all the necessary files next to it, but it's an accettable compromise. I was so happy, unaware of what was waiting for me...

## ACT 0x2: The Illusion of Reality
Let's do the same thing on windows! I just had  to substitute `/` with `\` in the pyinstaller cli command and that's it!

![It works, right?](/images/distribute1.png)

Double click on the .exe and.. it works just fine!

I was so excited to send the .zip to the customer,  but first I wanted to test it on a fresh install of windows 10, to check if this solution was truly portable and without dependencies of any kind.

I transferred the file on the new windows 10 VM and.. 

> windows defender quarantine notification

## ACT 0x3: The Harsh Truth 
Oh shit. Why? Why the fucking MS defender gets salty with my poor .exe?

## ACT 0xFF: The Solution