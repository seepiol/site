---
title: "📌 Resources"
date: 2022-11-10
summary: "A list of my resources and stuff I find interesting, on the internet. Constantly updated (if I remember)."

ShowReadingTime: false
ShowToc: true

draft: false

weight: 1
---

Here is a list of my resources and stuff I find interesting, on the internet.

Constantly updated (if I remember)

## Youtube Channels

### Stuff about computers

- [LiveOverflow](https://www.youtube.com/c/LiveOverflow): Probably the best youtube channel about security and hacking
    - [LiveUnderflow](https://www.youtube.com/channel/UCNNfzr9A5dEOscVEDyyzo-A): Second channel

- [Ben Eater](https://www.youtube.com/c/BenEater): Amazing explanations about electronic and low level programming

- [PwnFunction](https://www.youtube.com/c/PwnFunction): Entertaining videos about security and hacking (again)

- [Low Level Learinig](https://www.youtube.com/channel/UC6biysICWOJ-C3P4Tyeggzg): A lot of really good videos about low level programming 

- [John Hammond](https://www.youtube.com/c/JohnHammond010): CTF Video write ups and security related stuff (mostly on windows)

- [Engineer Man](https://www.youtube.com/c/EngineerMan): General videos about development and interesting examples

- [Daedalus Community](https://www.youtube.com/channel/UCuWLGQB4WRBKvW1C26zA2og): Series of video on how to create an OS

- [The Primeagen](https://www.youtube.com/channel/UC8ENHE5xdFSwx71u3fDH5Xw): Based developer that loves VIM and rust

- [Gavin Freeborn](https://www.youtube.com/user/g297125009): Linux & VIM

- [Andreas Kling](https://www.youtube.com/channel/UC3ts8coMP645hZw9JSD3pqQ): Creator of SerenityOS, lots of interesting videos about OS dev

- [Fireship](https://www.youtube.com/channel/UCsBjURrPoezykLs9EqgamOA): General videos about development

### Not just stuff about computers

- [Tom Scott](https://www.youtube.com/user/enyay): One of the best youtubers ever. 

- [rctestflight](https://www.youtube.com/user/rctestflight): Builds flying stuff

- [Chain Bear](https://www.youtube.com/user/chainbearf1): Technical videos about Formula 1

- [NASA Space Flight](https://www.youtube.com/channel/UCSUu1lih2RifWkKtDOJdsBA): Everything about spaceflight

- [Breaking Italy](https://www.youtube.com/user/breakingitaly): News (in italian)

- [74 Gear](https://www.youtube.com/c/74gear): Pilot who talks about aviation

- [Wendover Productions](https://www.youtube.com/c/Wendoverproductions): Excessively fascinating videos on the logistics of anything

- [Real Engineering](https://www.youtube.com/c/RealEngineering): Engineering of everything
