---
title: "📌 Hardware"
date: 2022-10-28
summary: "Hardware that I use"

ShowReadingTime: false

draft: false 

weight: 2
---

## Thinkpad T14 G1
![T14](/images/t14.png)
Bought in replacement of T430 (It still works, but a bit slow in cpu intensive operations), it's what i'm using as my main machine and it has an Intel i5-10210U and 8 gigs of RAM. I'm using it with Fedora 36 in a setup with i3wm and polybar. It isn't as robust as the t430, but it's a good laptop.

## Thinkpad T430
![ThinkCat](/images/thinkcat.jpg)

There is a lot of stuff to say about this machine, but the most important is that, even if it is 9 years old, it still rocks! 
On this incredible laptop I can do everything I need without any problem.
It has a 3rd gen i5 (i5-3320M) and 8Gb of RAM, 400GB of SSD + 1TB of HDD in the ultrabay.

It's quite an old computer, that's true, but it seemed to me the best choice. It's robust and durable, 100% compatible with linux, easy to upgrade and repair, the keyboard is amazing and the [ThinkLight](https://en.wikipedia.org/wiki/ThinkLight) is 1000 times more comfy than keyboard backlighting.

The quality of the screen is a bit _meh_, because it's a TN, but the majority of the time I use it with the docking station connected to an external monitor, so it isn't a big problem

## Raspberry Pi 4
I use this as a homelab: it's a [BitTorrent downloading server](https://www.qbittorrent.org/), a [Media Server](https://kodi.tv/), a backup server (I use [rsync](https://rsync.samba.org/)), a [SyncThing](https://syncthing.net/) node and it runs some docker images like [Nextcloud](https://nextcloud.com/), [NetData](https://www.netdata.cloud/) and I few other that I took from [LinuxServer.io](https://fleet.linuxserver.io/). I use it also for testing my projects.

## HP Compaq 6510b
![HP](/images/hp.jpg)


My first computer ever, pretty **t h i c c**. I don't really use it, but sometimes I use it to test the installation of some linux distros before installing them on the thinkpad. Ran [FreeBSD](https://www.freebsd.org/) for some time. 

## This Site

This site runs on a [Vultr](vultr.com) VPS. It's located in Paris, it has a 25GB SSD, 1GB of RAM and 1 virtual core. It runs Debian 10, and I use it for both personal and work projects

## Other 
- Lenovo Ideapad Z510: Windows Machine. I use it only for school stuff that requires that OS. It has a Nvidia GPU ([Nvidia, Fuck You!](https://www.youtube.com/watch?v=_36yNWw_07g)).

- Google Pixel 4a: I hate google, I don't even have a Google account anymore, but the phone is good. As soon as I got it I installed [Graphene OS](https://grapheneos.org/), a _privacy and security focused mobile OS with Android app compatibility developed as a non-profit open source project_.


