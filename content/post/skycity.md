---
title: "SkyCity"
date: 2024-02-13T22:52:36+01:00
draft: true
---

Starting in mid october 2023, I have been having fun building what I call "SkyCity": a mini homelab, with its own network, that lives in a drawer in my room.

## Hardware & Network

Since I became interested in networking and wanted to get my hands dirty on something real and not the usual emulated stuff, the first component of SkyCity was a router, a [MikroTik RouterBoard RB951Ui-2nD](https://mikrotik.com/product/RB951Ui-2nD). 

Even if it's cheap, not really powerful and ports are 10/100 instead of gigabit, is a really good device that runs the same exact os, RouterOS, of its more powerful counterparts, with same features and capabilities.

I connected the router (Called it KingdomGate because, you know, it's the gate to my kingdom, got it?) to the ISP provided router, and I configured it to act as a NAT gateway using NAT overload aka PAT.

In this way the devices on the ISP router network cannot reach internal SkyCity hosts, but internal hosts can use the interface of kingdomgate connected to the fastweb router in order to reach the internet. 
This configuration has minor drawbacks, including the fact that I cannot torrent **linux ISOs** anymore (I think because the torrent client cannot expose ports via upnp because of the nat, but I admit that I haven't studed really well this subject), but I plan to do something about it in the near future.

The second step was adding a server: I had a raspberry pi with some docker containers but that was too basic for the city in the sky: I bought from amazon renewed a [HP EliteDesk 800 g2 mini](https://support.hp.com/us-en/document/c04824956) with a i5 6500T and 8 gb of ram (Updated to 16) that runs Proxmox.

I then bought a shitty web-managed gigabit [zyxel switch](https://amzn.eu/d/9a7lAPp) in order to have a gigabit connection at least between my laptop and the proxmox server. 

My next device on the list is a gigabit wireless AP: having this, I would be able to transfer within a matter of seconds every photo that I take on my phone to the nextcloud instance on the elitedesk (more about this in the `Services & Applications` section)


## Services & Applications