---
title: "Photos"
date: 2021-08-17T23:33:28+02:00
ShowReadingTime: false
draft: true
---
### Some pics I took somewhere
#### Warning: This page could took a while to load because there are multiple uncompressed images

## Chiavari, Ligura
![7](/images/photos/chiavari/7.jpg)


![4](/images/photos/chiavari/4.jpg)


![3](/images/photos/chiavari/3.jpg)


![2](/images/photos/chiavari/2.jpg)

## Lago di Giacopiane, Borzonasca, Liguria
![1](/images/photos/chiavari/1.jpg)

## Milano
![3](/images/photos/milano/3.jpg)

![2](/images/photos/milano/2.jpg)

![2](/images/photos/milano/1.jpg)

