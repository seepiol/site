---
title: "ESP8266 Programming in C"
date: 2022-11-20T19:11:21+01:00
draft: true
showToc: true
tags: ["ESP8266", "Learning", "C"]
---

## Introduction

Some years ago I bought a pack of three NodeMCU [ESP8266 development boards](https://www.amazon.it/gp/product/B093G6N42D/). The core of the board is the ESP8266, a 32 bit RISC microchip that can use TCP/IP to communicate via Wi-Fi networks. I've always used them with arduino IDE, but [this video from Low Level Learning](https://www.youtube.com/watch?v=_dRrarmQiAM) caught my attention, and I've decided to dive a bit deeper. 

This post are my notes from watching the two videos I mentioned above and looking up things on the internet
- [Getting Started with the ESP32 Development Board | Programming an ESP32 in C/C++](https://www.youtube.com/watch?v=dOVjb2wXI84)
- [Getting Started with ESP32 Wireless Networking in C | Wirelessly Enable Any Project with ESP32](https://www.youtube.com/watch?v=_dRrarmQiAM)

## Prerequisites & Environment Setup
First of all, the videos are about ESP32, which is the successor of ESP8266. I think (and hope) that there aren't big differences, but in that case I'll figure out myself. I found this [programming guide directly from espressif](https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/get-started/index.html) that I'll follow.

### Toolchain Setup
The first step is to install on our machine the tools we will need to build the chip program. Here's the guide: [Standard Setup of Toolchain for Linux](https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/get-started/linux-setup.html)

#### Dependencies
Commands will be for Fedora Linux.
``` bash
sudo dnf install gcc git wget make ncurses-devel flex bison gperf python python-pyserial 
```

#### Download
Download the ESP8266 from the espressif site into `$HOME/esp`
```bash
mkdir esp
cd esp
wget https://dl.espressif.com/dl/xtensa-lx106-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz
```
Extract the content
```bash
tar -xzf xtensa-lx106-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz 
```

#### Add to PATH
Add the toolchain to path. I'm using ZSH so I'll need to add `.zshrc` the following line
```bash
export PATH="/home/user/esp/xtensa-lx106-elf/bin:$PATH"
```

### SDK setup
In order to write programs for ESP8266 we need the chip specific libraries.

#### Download
We can get them by cloning the [ESP8266_RTOS_SDK](https://github.com/espressif/ESP8266_RTOS_SDK.git) repository, always into the `/esp` folder.
```bash
cd esp
git clone --recursive https://github.com/espressif/ESP8266_RTOS_SDK.git
```

#### Add to PATH
The tools contained into the toolchains access to the SDK libraries by using the `IDF_PATH` environment variable. If we want to make this permanent, as we did before, open `.zshrc` and add 
```
export IDF_PATH="/home/user/esp/ESP8266_RTOS_SDK"
```

#### Install Python Dependencies
```
python -m pip install --user -r $IDF_PATH/requirements.txt
```

## New Project

As the documentation suggests, we'll use the hello world example. 
```
cd ~/esp
cp -r $IDF_PATH/examples/get-started/hello_world .
```