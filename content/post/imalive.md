---
title: "I'm Alive!"
date: 2024-04-04T13:30:00+01:00
---

{{< goodiframe >}}

Hi!
After a huge period of inactivity (almost as if it could be said that this blog, somewhere in the past, had ever been active) I decided to clone the repo of this site and update it. Please don't go to About Me until I update it because almost everything is deprecated at this point.

A lot has happened: I graduated from high school and got the driver license (on the same day), new amazing people entered my life, I started to work on a new "freelance" project, I studied networking at [eForHum](https://www.eforhum.it/) joining the [Junior IT Academy](https://www.junioritacademy.it/), where I met incredibly passionate and skilled people, took my CCNA 200-301 (Here's the [proof](https://www.credly.com/badges/fc679666-559a-4876-ae76-dcb4f00deeb0/public_url), btw) and got an introduction into the world of virtualization (VMware vSphere ICM v8) and network security (Palo Alto ESD & ESM). 

After the end of the classes I met several companies and had two inteviews (which were extremely valuable in understanding the professional world and what is waiting for me).

Both companies accepted me. The choice was up to me: I had to choose between a carrer start focused on networking, or being able to join a Red Team, starting right away to work in the field of offensive cybersecurity, the field that has fascinated me the most since high school.

The choice was hard, but in the end I decided to go for the second option. In a few days I will start working as a Cybersecurity Engineer! Hell yeah!

In the meantime of all this I have also built in the drawer of my room a sort of homelab, but I plan to write a post just about it.

That's it! It's really weird for me to recap the last 10 months in 220 words, also because of all the things that have happened in my life, both personal and professional/techy/you name it, these 10 months seem to have lasted years under some aspects, and hours under others.

I don't really know how I will be in a month, so many things are in progress. I just hope that all these new stuff will push me to spend at least a bit of my time to this site/dump/blog!

`RET`