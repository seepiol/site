---
title: "Astropointer"
date: 2021-09-09T12:07:15+02:00
summary: "A cool project (useless, but who cares?)"
tags: ["Project", "ESP8266"]
---

## September, 9 Update
It's been less than a week, but I've made huge progress with astropointer.
First of all, now it has 3 different button: an **Activation Button** that needs to be pressed to make the cursor move, and the two clicks uttons. The click cannot be used while the mouse is moving, but it's just an implementation decison.

One necessary feature I haven't worked on yet is the drag funcionality, which will be more trivial.

Another huge feature I've added is the wireless mode. I started working on that this afternoon and it works surprisingly well given the time. The way it works is really simple: instead of sending the gyroscope readings on the serial interface, the ESP sends them using a TCP socket. The latency is quite good, but it's kinda sluggish in the movementsbecause not every gyroscope read reaches the client.

There are some stuff I can do in order to increase the smoothness, like using !python for the reciver, using a UDP socket instead of TCP, stuff like that, but overall I'm happy about I managed to do in a week.

Another problem is that reaching the screen corners is really difficult, but this is because of the gyroscope. Probably some hacking with the coordinate calculation algorithm should help me to get me out of this.

I will post photos, videos and schematics as soon as I can.

---
## September, 4 
9 days before the start of school, because of course I am absolutely ready (no, I'm not), I decided to create this thing:

{{<rawhtml>}} 
<video width=100% controls autoplay>
    <source src="/videos/demos/astropointer_demo_1.mp4" type="video/mp4">
        Your browser does not support the video tag.  
</video>
{{</rawhtml>}}

It **should** be a pointer device that uses gyroscope and accellerometer in order to know where the cursor should be.

Of course it's a huge work in progress, but if you want to see the (shitty and unsafe) code, here you go: [Gitlab Repository](https://gitlab.com/seepiol/astro)
