---
title: "About Me"
showToc: false
tocOpen: false
disableShare: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowToc: true
hideMeta: true
draft: false
---

Hi. I'm Jacopo Moioli, a 19 years old computer science student.

In my freetime I write code, so I guess _technically_ I can be defined as a developer.

I use a lot of different technologies, and I'm constantly looking for new things that allow me to improve what I do: from the text editor I use to the way I deploy my projects.

I think that, at least in the digital world, everyone should have the opportunity to access the technology without limitations or barriers of any kind, but most importantly, in an indipendent way. I support the open source movement because I think the open source development model makes the principle above a reality.

In addition to computer and programming, another sort of passion of mine is space exploration. Thinking that up there in the solar system and beyond there is hardware created by a human being (**like me!**) immensely fascinates and inspires me. 
I hope with my whole heart that, during my life, I can see another human being walking on another planet, different from earth.
Specifically, I constantly follow the development of the [SpaceX](https://spacex.com)'s [Starship launch system](https://twitter.com/austinbarnard45/status/1423633746020929538), practically since the [first hop of starhopper](https://www.youtube.com/watch?v=bYb3bfA6_sQ) or probably even earlier.

I listen to music, and in this period of time I'm listening to a lot of Kanye West. This is my [Spotify](https://open.spotify.com/user/sotd321u4hkchdmppynva2u72).

## Stuff I work with 

### Linux
As I said above, I prefer open source software. It is a given then that rather than using proprietary operating systems such as Windows or Mac Os, I use Linux (btw)

I've tried almost every single linux distribution, but now, on my daily driver, I'm ok with [Fedora](https://getfedora.org/).
In the future I would like to try [NixOS](https://nixos.org/), because I'm curious about its declarative configuration and its package manager.

On servers I use debian, but I would like to do the switch to [Rocky Linux](https://rockylinux.org/) because of its binary compatibility with RHEL (Red Hat Enterprise Linux).

The daily use has taught me useful notions about the management of a linux system, that I can apply both in a desktop and in a server environment.

If you want to know more about the hardware I work on, I talk about it [here](/post/hardware)


### [Python](https://www.python.org/)
Python is an _interpreted high-level general-purpose programming language_.
I like it because its syntax is clear, the community about it is enormous, there are a ton of libraries for doing almost everything, and only with it alone I can develop everything, from CLI programs, to desktop applications, from microcontroller programming with [MicroPython](http://micropython.org/) to web-app development with [Django](https://www.djangoproject.com/)

#### [Django](https://www.djangoproject.com)
Django, as I said above, is a python based web framework.
It allowed me to develop several web apps with ease, making myself take care only of the business-logic of my project rather than of the more common but also more difficult to develop functions such as user management and authentication. 

#### [Flask](https://flask.palletsprojects.com/en/2.0.x/)
Flask is another framework, or more precisely a microframework, for web development, written in python. I used it to develop minimal web apps that wouldn't have justified the use of a full-featured framework like django.

#### [FastAPI](https://fastapi.tiangolo.com/)
FastAPI is a web framework for building APIs. It's really simple and painless, and thanks to its type annotation ([PyDantic](https://docs.pydantic.dev/)) writing bugs is a bit harder.

### [Java](https://en.wikipedia.org/wiki/Java_(programming_language))
I've used java for the development of various desktop programs with GUIs (made with [JavaFX](https://openjfx.io/)).
I've studied it at school and I'm currently learing about [JSP](https://www.oracle.com/java/technologies/jspt.html) (JavaServer Pages)

### [Go](https://go.dev/)
Go is a relatively recent programming language, open source but supported by Google (bruh). I haven't done anything huge but I'm enjoying it a lot: it's a good compromise between a low level language like C and something like Python, and the concurrency is handled in an unbelievably good way with [GoRoutines](https://go.dev/tour/concurrency/1). I've used it to solve [Advent of Code 2022](https://adventofcode.com/2022) problems.

#### [Gin Web Framework](https://gin-gonic.com/)
Gin is a Go framework for the development of web services. I used it to write some REST APIs


### Various stuff & other programming languages I've used
- [Flutter](https://flutter.dev/): a multiplatform framework for mobile app development. I've used it to develop an application for android but I don't really love it
- [Git](https://git-scm.com/): I use git every single day, so I can keep track of every single change I make to my projects
- [Docker](https://www.docker.com/) and Docker Compose: I use them both during development and in production, to use more services on the same machine without messing with the host system. Both with docker from cli and [Portainer](https://www.portainer.io/)
- Bash Scripts: Due to my lazy nature, I've written many bash scripts to automate what I need to do but I don't want to do every time manually
- SQL databases ([Postgresql](https://www.postgresql.org/), [Sqlite](https://sqlite.org/index.html))
- [NGINX](https://nginx.org/en/): Web server and reverse proxy for containerized services.
- [MinIO](https://min.io/): Selfhosted object storage, compatible with AWS S3.
- [Traefik](https://traefik.io/traefik/): Reverse proxy specifically made for containers.
- [Apache Kafka](https://kafka.apache.org/): A distributed event streaming platform. I'm learning it in order to develop the back-end of a quite big project.
- [Radare2](https://github.com/radareorg/radare2): an incredibly complex but awesome open source framework for binary reverse engineering. I can barely say I know the basics, and I would like to use it much more than I do now
- [Ghidra](https://ghidra-sre.org/): something similar to radare, but with a proper GUI and developed by NSA
- C 
- Rust
- Assembly x86 
- HTML & CSS (even if I prefer back-end stuff)
- Hugo (used to make this site)
- Ruby

